package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        city=findViewById(R.id.editText);
        temp=findViewById(R.id.textView);
        tv2=findViewById(R.id.textView5);
        tv3=findViewById(R.id.textView6);
        tv4=findViewById(R.id.textView7);
        tv6=findViewById(R.id.textView8);

    }
    EditText city;

    TextView temp;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv6;

    public  void City(){
        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("main");
                    String temp2 = String.valueOf(main.getInt("temp"));
                    temp.setText(temp2.toString()+"°C");
                } catch (JSONException e) {temp.setText(e.getMessage()); } }}, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) { temp.setText(error.getMessage());}});
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
        jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("main");
                    String feel_like = String.valueOf(main.getDouble("feels_like"));
                    tv2.setText("Ощущается : "+feel_like.toString()+"°C");
                } catch (JSONException e) {tv2.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { tv2.setText(error.getMessage());}});
        RequestQueue requestQueue1 = Volley.newRequestQueue(this);
        requestQueue1.add(jo);

        jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String visibility = String.valueOf(response.getInt("visibility")/1000);
                    tv3.setText("Видимость : "+visibility+" Км");
                } catch (JSONException e) {tv3.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { tv3.setText(error.getMessage());}});
        RequestQueue requestQueue2 = Volley.newRequestQueue(this);
        requestQueue2.add(jo);
        jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("wind");
                    String speed = String.valueOf(main.getDouble("speed"));
                    String deg = String.valueOf(main.getInt("deg"));
                    tv4.setText("Скорость : "+speed.toString() + " м/с");
                } catch (JSONException e) {tv4.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { tv4.setText(error.getMessage());}});
        RequestQueue requestQueue3 = Volley.newRequestQueue(this);
        requestQueue3.add(jo);

        String url1 = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";

        jo = new JsonObjectRequest(Request.Method.GET, url1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray weather = response.getJSONArray("weather");
                    JSONObject index = weather.getJSONObject(0);
                    String description=index.getString("description");
                    tv6.setText("Тип : "+description.toString());
                } catch (JSONException e) {tv6.setText(e.getMessage()); } }}, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) { tv6.setText(error.getMessage());}});
        RequestQueue requestQueue5 = Volley.newRequestQueue(this);
        requestQueue5.add(jo);

}

    public void click(View view) {
        City();
    }
    }

